package net.powem.atmapppower;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AtmappApplicationTests {
	private User user;
	private AtmApplication atmApplicationSpy;

	//constants
	private float maxDepositAmountForTheDay = 150000;
	private int maxDepositFrequency = 4;
	private float maxDepositPerTransaction = 40000;

	private float maxWithdrawalAmountForTheDay = 50000;
	private int maxWithdrawalFrequency = 3;
	private float maxWithdrawalPerTransaction = 20000;
	@Rule
	public MockitoRule mrule = MockitoJUnit.rule();

	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		user = new User(100,0,0,0,0);
		atmApplicationSpy = spy(AtmApplication.class);
	}


	@Test
	public void testInitApp() {
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.initApp();
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testShowMainMenu_whenUserEntersBalance_handleBalanceInstructionIsCalled() {
		doReturn("BALANCE").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).handleBalanceInstruction();
		atmApplicationSpy.showMainMenu();
		verify(atmApplicationSpy).handleBalanceInstruction();
	}

	@Test
	public void testShowMainMenu_whenUserEntersDeposit_handledDepositInstructionIsCalled() {
		doReturn("DEPOSIT").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).handleDepositInstruction();
		atmApplicationSpy.showMainMenu();
		verify(atmApplicationSpy).handleDepositInstruction();
	}

	@Test
	public void testShowMainMenu_whenUserEntersWithdrawal_handledWithdrawalInstructionIsCalled() {
		doReturn("WITHDRAWAL").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).handleWithdrawalInstruction();
		atmApplicationSpy.showMainMenu();
		verify(atmApplicationSpy).handleWithdrawalInstruction();
	}

	@Test
	public void testShowMainMenu_whenUserEntersQuit_handledQuitInstructionIsCalled() {
		doReturn("QUIT").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).handleQuitInstruction();
		atmApplicationSpy.showMainMenu();
		verify(atmApplicationSpy).handleQuitInstruction();
	}

	/*
	*********************************************Balance: START***************************************************************
 	*/

	@Test
	public void testHandleBalanceInstruction_whenMenuOptionIsEntered_showMainMenuCalled() {
		atmApplicationSpy.setUser(user);
		doReturn("MENU").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleBalanceInstruction();
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testHandleBalanceInstruction_whenInvalidInputIsEntered_showErrorIsCalled_showMainMenuCalled() {
		atmApplicationSpy.setUser(user);
		doReturn("MUKISA").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleBalanceInstruction();
		verify(atmApplicationSpy).showErrorWithoutMenuMesage(anyString());
		verify(atmApplicationSpy).showMainMenu();
	}
	/*
	*********************************************Balance: END***************************************************************
	 */


	/*
	*********************************************Deposit: START***************************************************************
	 */


	@Test
	public void testHandleDepositInstruction_whenMaxDepositForTheDayExceeded_andMaxDepositFrequencyExceeded_showCurrentBalance_handleDepositAfterValidation() {
		atmApplicationSpy.setUser(user);
		doReturn(false).when(atmApplicationSpy).maxDepositForTheDayExceeded();
		doReturn(false).when(atmApplicationSpy).maxDepositFrequencyExceeded();
		doNothing().when(atmApplicationSpy).handleDepositAfterValidation();
		atmApplicationSpy.handleDepositInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).handleDepositAfterValidation();
	}

	@Test
	public void testHandleDepositInstruction_whenMaxDepositForTheDayNotExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn(true).when(atmApplicationSpy).maxDepositForTheDayExceeded();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testHandleDepositInstruction_whenMaxDepositForTheDayExceeded_andMaxDepositFrequencyNotExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn(false).when(atmApplicationSpy).maxDepositForTheDayExceeded();
		doReturn(true).when(atmApplicationSpy).maxDepositFrequencyExceeded();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleDepositAfterValidation_whenInputIsANumber_andMaxDepositPerTransactionNotExceeded_userObjectUpdated_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("100").when(atmApplicationSpy).listenForInput();
		doReturn(false).when(atmApplicationSpy).maxDepositPerTransactionExceeded("100");
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositAfterValidation();
		assert(user.getAmountDepositedToday()==100.0);
		assert(user.getNumberOfTimeUserHasDepositedToday()==1);
		assert(user.getBalance()==200.0);
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleDepositAfterValidation_whenInputIsANumber_andMaxDepositPerTransactionExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("100").when(atmApplicationSpy).listenForInput();
		doReturn(true).when(atmApplicationSpy).maxDepositPerTransactionExceeded("100");
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositAfterValidation();
		assert(user.getAmountDepositedToday()==0.0);
		assert(user.getNumberOfTimeUserHasDepositedToday()==0);
		assert(user.getBalance()==100.0);
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testHandleDepositAfterValidation_whenInputIsMenu_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("MENU").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositAfterValidation();
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleDepositAfterValidation_whenInputIsNotMenuOrANumber_showErrorWithoutMenuMesage_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("SKKRRRR").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleDepositAfterValidation();
		verify(atmApplicationSpy).showErrorWithoutMenuMesage(anyString());
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testMaxDepositForTheDayExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		user.setAmountDepositedToday(maxDepositAmountForTheDay);
		boolean rtnValue = atmApplicationSpy.maxDepositForTheDayExceeded();
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxDepositForTheDayExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		user.setAmountDepositedToday(20000);
		boolean rtnValue = atmApplicationSpy.maxDepositForTheDayExceeded();
		assertFalse(rtnValue);
	}

	@Test
	public void testMaxDepositPerTransactionExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.maxDepositPerTransactionExceeded(String.valueOf(maxDepositPerTransaction+1));
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxDepositPerTransactionExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.maxDepositPerTransactionExceeded(String.valueOf(maxDepositPerTransaction-1));
		assertFalse(rtnValue);
	}

	@Test
	public void testMaxDepositFrequencyExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		user.setNumberOfTimeUserHasDepositedToday(maxDepositFrequency);
		boolean rtnValue = atmApplicationSpy.maxDepositFrequencyExceeded();
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxDepositFrequencyExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		user.setNumberOfTimeUserHasDepositedToday(maxDepositFrequency-1);
		boolean rtnValue = atmApplicationSpy.maxDepositFrequencyExceeded();
		assertFalse(rtnValue);
	}

	/*
	*********************************************Deposit: END***************************************************************
	 */



	/*
	*********************************************Withdrawal: START***************************************************************
	 */

	@Test
	public void testHandleWithdrawalInstruction_whenMaxDepositForTheDayExceeded_andMaxDepositFrequencyExceeded_showCurrentBalance_handleDepositAfterValidation() {
		atmApplicationSpy.setUser(user);
		doReturn(false).when(atmApplicationSpy).maxWithdrawalForTheDayExceeded();
		doReturn(false).when(atmApplicationSpy).maxWithdrawalFrequencyExceeded();
		doNothing().when(atmApplicationSpy).handleWithdrawalAfterValidation();
		atmApplicationSpy.handleWithdrawalInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).handleWithdrawalAfterValidation();
	}

	@Test
	public void testHandleWithdrawalInstruction_whenMaxDepositForTheDayNotExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn(true).when(atmApplicationSpy).maxWithdrawalForTheDayExceeded();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testHandleWithdrawalInstruction_whenMaxWithdrawalForTheDayExceeded_andMaxWithdrawalFrequencyNotExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn(false).when(atmApplicationSpy).maxWithdrawalForTheDayExceeded();
		doReturn(true).when(atmApplicationSpy).maxWithdrawalFrequencyExceeded();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalInstruction();
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleWithdrawalAfterValidation_whenInputIsANumber_andMaxWithdrawalPerTransactionNotExceeded_userObjectUpdated_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("100").when(atmApplicationSpy).listenForInput();
		doReturn(false).when(atmApplicationSpy).maxWithdrawalPerTransactionExceeded("100");
		doReturn(true).when(atmApplicationSpy).balanceMoreThanWithdrawAmount("100");
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalAfterValidation();
		assert(user.getAmountWithdrawnToday()==100.0);
		assert(user.getNumberOfTimeUserHasWithdrawnToday()==1);
		assert(user.getBalance()==0.0);
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleWithdrawalAfterValidation_whenInputIsANumber_andMaxWithdrawalPerTransactionExceeded_showCurrentBalance_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("100").when(atmApplicationSpy).listenForInput();
		doReturn(true).when(atmApplicationSpy).maxWithdrawalPerTransactionExceeded("100");
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalAfterValidation();
		assert(user.getAmountWithdrawnToday()==0.0);
		assert(user.getNumberOfTimeUserHasWithdrawnToday()==0);
		assert(user.getBalance()==100.0);
		verify(atmApplicationSpy).showCurrentBalance();
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testHandleWithdrawalAfterValidation_whenInputIsMenu_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("MENU").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalAfterValidation();
		verify(atmApplicationSpy).showMainMenu();
	}

	@Test
	public void testHandleWithdrawalAfterValidation_whenInputIsNotMenuOrANumber_showErrorWithoutMenuMesage_showMainMenu() {
		atmApplicationSpy.setUser(user);
		doReturn("SKKRRRR").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleWithdrawalAfterValidation();
		verify(atmApplicationSpy).showErrorWithoutMenuMesage(anyString());
		verify(atmApplicationSpy).showMainMenu();
	}


	@Test
	public void testMaxWithdrawalForTheDayExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		user.setAmountWithdrawnToday(maxWithdrawalAmountForTheDay);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalForTheDayExceeded();
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxWithdrawalForTheDayExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		user.setAmountWithdrawnToday(20000);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalForTheDayExceeded();
		assertFalse(rtnValue);
	}

	@Test
	public void testMaxWithdrawalPerTransactionExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalPerTransactionExceeded(String.valueOf(maxWithdrawalPerTransaction+1));
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxWithdrawalPerTransactionExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalPerTransactionExceeded(String.valueOf(maxWithdrawalPerTransaction-1));
		assertFalse(rtnValue);
	}

	@Test
	public void testBalanceMoreThanWithdrawAmount_whenMore_returnTrue() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.balanceMoreThanWithdrawAmount(String.valueOf(user.getBalance()-1));
		assertTrue(rtnValue);
	}


	@Test
	public void testBalanceMoreThanWithdrawAmount_whenLess_returnFalse() {
		atmApplicationSpy.setUser(user);
		boolean rtnValue = atmApplicationSpy.balanceMoreThanWithdrawAmount(String.valueOf(user.getBalance()+1));
		assertFalse(rtnValue);
	}

	@Test
	public void testMaxWithdrawalFrequencyExceeded_whenExceeded_returnTrue() {
		atmApplicationSpy.setUser(user);
		user.setNumberOfTimeUserHasWithdrawnToday(maxWithdrawalFrequency);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalFrequencyExceeded();
		assertTrue(rtnValue);
	}


	@Test
	public void testMaxWithdrawalFrequencyExceeded_whenExceeded_returnFalse() {
		atmApplicationSpy.setUser(user);
		user.setNumberOfTimeUserHasWithdrawnToday(maxWithdrawalFrequency-1);
		boolean rtnValue = atmApplicationSpy.maxWithdrawalFrequencyExceeded();
		assertFalse(rtnValue);
	}

	/*
	*********************************************Withdrawal: END***************************************************************
	 */



	/*
	*********************************************Quit: START***************************************************************
	 */

	@Test
	public void testHandleQuitInstruction_whenInputIsY_quitProgram() {
		doReturn("Y").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).quitProgram();
		atmApplicationSpy.handleQuitInstruction();
		verify(atmApplicationSpy).quitProgram();
	}


	@Test
	public void testHandleQuitInstruction_whenInputIsN_showMainMenu() {
		doReturn("N").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleQuitInstruction();
		verify(atmApplicationSpy).showMainMenu();
	}



	@Test
	public void testHandleQuitInstruction_whenInputIsInvalid_showMainMenu() {
		doReturn("MAN'S NOT HOT").when(atmApplicationSpy).listenForInput();
		doNothing().when(atmApplicationSpy).showMainMenu();
		atmApplicationSpy.handleQuitInstruction();
		verify(atmApplicationSpy).showErrorWithoutMenuMesage(anyString());
		verify(atmApplicationSpy).showMainMenu();
	}

	/*
	*********************************************Quit: END***************************************************************
	 */



}
