package net.powem.atmapppower;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Scanner;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class UserServiceTests {
	User user = new User(200000,0,0,0,0);

	@Rule
	public MockitoRule mrule = MockitoJUnit.rule();

	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);

	}


	@Test
	public void testGetUser() {
		UserService service = spy(UserService.class);
		service.setUser(user);
		User returnedUser;
		returnedUser = service.getUser();
		assert(returnedUser == user);
	}

}
