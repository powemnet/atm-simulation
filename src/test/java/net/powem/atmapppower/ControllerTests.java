package net.powem.atmapppower;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Scanner;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ControllerTests {

	@Rule
	public MockitoRule mrule = MockitoJUnit.rule();

	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);

	}


	@Test
	public void testGetUser() {
		UserService service = mock(UserService.class);
		Controller controller = spy(Controller.class);
		controller.setService(service);
		controller.getUser();
		verify(service).getUser();
	}


}
