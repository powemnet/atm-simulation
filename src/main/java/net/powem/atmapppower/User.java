package net.powem.atmapppower;

public class User {

    private float balance;
    private float amountDepositedToday;
    public int numberOfTimeUserHasDepositedToday;
    private float amountWithdrawnToday;
    public int numberOfTimeUserHasWithdrawnToday;


    public User(){

    }


    public User(float balance, float amountDepositedToday, int numberOfTimeUserHasDepositedToday, float amountWithdrawnToday, int numberOfTimeUserHasWithdrawnToday){
        this.balance = balance;
        this.amountDepositedToday = amountDepositedToday;
        this.amountWithdrawnToday = amountWithdrawnToday;
    }

    public void setBalance(float balance){
        this.balance = balance;
    }

    public float getBalance() {
        return balance;
    }

    public void setAmountDepositedToday(float amountDepositedToday){
        this.amountDepositedToday = amountDepositedToday;
    }

    public float getAmountDepositedToday() {
        return amountDepositedToday;
    }

    public void setNumberOfTimeUserHasDepositedToday(int  numberOfTimeUserHasDepositedToday){
        this.numberOfTimeUserHasDepositedToday = numberOfTimeUserHasDepositedToday;
    }

    public int  getNumberOfTimeUserHasDepositedToday() {
        return numberOfTimeUserHasDepositedToday;
    }

    public void setAmountWithdrawnToday(float amountWithdrawnToday){
        this.amountWithdrawnToday = amountWithdrawnToday;
    }

    public float getAmountWithdrawnToday() {
        return amountWithdrawnToday;
    }

    public void setNumberOfTimeUserHasWithdrawnToday(int  numberOfTimeUserHasWithdrawnToday){
        this.numberOfTimeUserHasWithdrawnToday = numberOfTimeUserHasWithdrawnToday;
    }

    public int  getNumberOfTimeUserHasWithdrawnToday() {
        return numberOfTimeUserHasWithdrawnToday;
    }


}
