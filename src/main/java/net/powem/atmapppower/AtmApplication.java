package net.powem.atmapppower;

import java.util.Scanner;

public class AtmApplication {

	private UserService userService;
	private Controller controller;
	private User user;
	private String input;
	private Scanner scanner;

	//constants
	private float maxDepositAmountForTheDay = 150000;
	private int maxDepositFrequency = 4;
	private float maxDepositPerTransaction = 40000;

	private float maxWithdrawalAmountForTheDay = 50000;
	private int maxWithdrawalFrequency = 3;
	private float maxWithdrawalPerTransaction = 20000;

	private String menuMessage = "Type menu and press enter to go back to main menu";


	public static void main(String[] args) {
		AtmApplication atmApplication = new AtmApplication();
		atmApplication.initApp();

	}

	public void initApp()   {
		userService = new UserService();
		//initialise controller
		controller = new Controller(userService);
		user = controller.getUser();

		//initialise console scanner
		scanner = new Scanner(System.in);

		showMainMenu();
	}



	/*
	*********************************************Main Menu: START***************************************************************
	 */

	public void showMainMenu() {

		System.out.println("*************************************************************************************************************");
		System.out.println("	1. BALANCE");
		System.out.println("	2. DEPOSIT");
		System.out.println("	3. WITHDRAWAL");
		System.out.println("	4. QUIT");
		System.out.println("Enter Menu Option ");

		input = listenForInput();

		if(inputEqualsAnyOfTheMainOptions(input)) {
			if(input.equals("BALANCE")){
				handleBalanceInstruction();
			}

			else if(input.equals("DEPOSIT")){

				handleDepositInstruction();

			}

			else if(input.equals("WITHDRAWAL")){

				handleWithdrawalInstruction();

			}

			else if(input.equals("QUIT")){

				handleQuitInstruction();

			}
		}else {
			showErrorWithoutMenuMesage("Invalid Input");
			showMainMenu();
		}

	}

	private boolean inputEqualsAnyOfTheMainOptions(String input) {
		return input.equals("BALANCE")||input.equals("DEPOSIT")||input.equals("WITHDRAWAL")||input.equals("QUIT");
	}






	/*
	*********************************************Main Menu: END***************************************************************
	 */






	/*
	*********************************************Balance: START***************************************************************
 	*/
	public void handleBalanceInstruction() {
		System.out.println(user.getBalance());
		System.out.println("Type “menu” and press enter to go back to main menu ");
		input = listenForInput();

		if (inputIsMenu(input)){
			showMainMenu();
		}
		else {
			showErrorWithoutMenuMesage("Invalid Input");
			showMainMenu();
		}

	}


	/*
	*********************************************Balance: END***************************************************************
	 */




	/*
	*********************************************Deposit: START***************************************************************
	 */

	public void handleDepositInstruction() {
		if(!maxDepositForTheDayExceeded()){
                        if(!maxDepositFrequencyExceeded()){
                            showCurrentBalance();
                            System.out.println("Enter amount and press enter (or type menu and press enter to go back to main menu)");
                            handleDepositAfterValidation();

                        }else{
                            showCurrentBalance();
							showErrorWithoutMenuMesage("Sorry, you cannot deposit more than 4 times in a day");
							showMainMenu();
                        }


        }else {
            showCurrentBalance();
            showErrorWithoutMenuMesage("Sorry, you can not deposit more than 150,000 in a day");
            showMainMenu();
        }
	}

	public void handleDepositAfterValidation() {
		input = listenForInput();
		if(inputIsANumber(input)){

			if(!maxDepositPerTransactionExceeded(input)){
				user.setAmountDepositedToday(user.getAmountDepositedToday()+Float.valueOf(input));
				user.setNumberOfTimeUserHasDepositedToday(user.getNumberOfTimeUserHasDepositedToday()+1);
				user.setBalance(user.getBalance()+Float.valueOf(input));
				showMainMenu();

		}
		else{
			showCurrentBalance();
			showErrorWithoutMenuMesage("Sorry, you can not deposit more than 40,000 in a single transaction");
			showMainMenu();
		}

		}
		else if (inputIsMenu(input)){
			showMainMenu();
		}
		else {
			showErrorWithoutMenuMesage("Invalid Input");
			showMainMenu();
		}

	}


	public boolean maxDepositForTheDayExceeded() {
		return user.getAmountDepositedToday()>= maxDepositAmountForTheDay;
	}

	public boolean maxDepositPerTransactionExceeded(String input) {
		return Float.valueOf(input)> maxDepositPerTransaction;
	}

	public boolean maxDepositFrequencyExceeded() {
		return user.getNumberOfTimeUserHasDepositedToday()>= maxDepositFrequency;

	}

	/*
	*********************************************Deposit: END***************************************************************
	 */



	/*
	*********************************************Withdrawal: START***************************************************************
	 */


	public void handleWithdrawalInstruction() {

		if(!maxWithdrawalForTheDayExceeded()){
			if(!maxWithdrawalFrequencyExceeded()){
				showCurrentBalance();
				System.out.println("Enter amount and press enter (or type menu and press enter to go back to main menu)");
				handleWithdrawalAfterValidation();

			}else{
				showCurrentBalance();
				showErrorWithoutMenuMesage("Sorry, you cannot withdraw more than 3 times in a day");
				showMainMenu();
			}


		}else {
			showCurrentBalance();
			showErrorWithoutMenuMesage("Sorry, you can not withdraw more than 50000 in a day");
			showMainMenu();
		}

	}


	public void handleWithdrawalAfterValidation() {
		input = listenForInput();
		if(inputIsANumber(input)){

			if(!maxWithdrawalPerTransactionExceeded(input)){

				if(balanceMoreThanWithdrawAmount(input)){
					user.setAmountWithdrawnToday(user.getAmountWithdrawnToday()+Float.valueOf(input));
					user.setNumberOfTimeUserHasWithdrawnToday(user.getNumberOfTimeUserHasWithdrawnToday()+1);
					user.setBalance(user.getBalance()-Float.valueOf(input));
					showMainMenu();
				}
				else {
					showCurrentBalance();
					showErrorWithoutMenuMesage("Sorry, you account balance is too low");
					showMainMenu();
				}


			}
			else{
				showCurrentBalance();
				showErrorWithoutMenuMesage("Sorry, you can not withdraw more than 20,000 in a single transaction");
				showMainMenu();
			}

		}
		else if (inputIsMenu(input)){
			showMainMenu();
		}
		else {
			showErrorWithoutMenuMesage("Invalid Input");
			showMainMenu();
		}

	}

	public boolean balanceMoreThanWithdrawAmount(String input) {
		return user.getBalance()>Float.valueOf(input);
	}

	public boolean maxWithdrawalForTheDayExceeded() {
		return user.getAmountWithdrawnToday()>= maxWithdrawalAmountForTheDay;
	}

	public boolean maxWithdrawalPerTransactionExceeded(String input) {
		return Float.valueOf(input)> maxWithdrawalPerTransaction;
	}

	public boolean maxWithdrawalFrequencyExceeded() {
		return user.getNumberOfTimeUserHasWithdrawnToday()>= maxWithdrawalFrequency;

	}


	/*
	*********************************************Withdrawal: END***************************************************************
	 */

	/*
	*********************************************Quit: START***************************************************************
	 */


	public void handleQuitInstruction() {
		System.out.println("Are you sure you want to quit? (Y/N)");
		input = listenForInput();
		if(input.equals("Y")){
			quitProgram();
		}
		else if (input.equals("N")){
			showMainMenu();
		}
		else {
			showErrorWithoutMenuMesage("Invalid Input");
			showMainMenu();
		}
	}

	public void quitProgram() {
		System.out.println("Ok bye!");
		scanner.close();
		System.exit(0);
	}



	/*
	*********************************************Quit: END***************************************************************
	 */



	/*
	*************************************Generic Methods: START******************************************
	 */
	public void showError(String error) {
		System.out.println("Error: "+error+" "+menuMessage);
	}

	public void showErrorWithoutMenuMesage(String error) {
		System.out.println("Error: "+error);
	}

	public boolean inputIsANumber(String input) {
		return input.matches("-?\\d+(\\.\\d+)?");
	}

	public void showCurrentBalance() {
		System.out.println("Current Balance: "+user.getBalance());

	}

	public String listenForInput() {
		return scanner.next().toUpperCase();
	}

	public boolean inputIsMenu(String input) {
		return input.equals("MENU");
	}

	/*
	*************************************Generic Methods: END******************************************
	 */

	//setters for the sake of testing
	public void setUser(User user) {
		this.user = user;
	}
}
