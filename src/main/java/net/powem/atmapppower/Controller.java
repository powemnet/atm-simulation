package net.powem.atmapppower;


public class Controller {

    private UserService userService;

    public Controller() {

    }

    public Controller(UserService userService) {
        this.userService = userService;
    }

    public User getUser()   {
        return userService.getUser();
    }

    public void setService(UserService service){
        userService = service;
    }
}
