**ATM Simulation by Power Mukisa**

A simple plain java applicaiton.
Follow the instructions below to try it out :)
---

## Clone this repo

1. Donwload the repo and cd into the root directory

---

## Install dependencies

Next, you�ll add a new file to this repository.

1. Install JDK 8 
2. Run export JAVA_OPTS=-Xmx1024m -XX:MaxPermSize=128M
3. Install gradle with the command *sudo apt install gradle* (for linux)
4. You may need to run *gradle wrapper* to set up graddle wrapper

---

**All the following instructions need to be run in the project root directory**

## Run the program  

First Option : Create the jar file and run it   

1. Run *./gradlew clean* 
2. Run *./gradlew fatJar*
3. Go to build/libs/
4. Run *java -jar atmapp-all-1.0-SNAPOT.jar*

Second Option:    

1. Run the command *./gradlew run* 


---

## Run the tests 

1. Run the command *./gradlew clean build*
2. Find the test results under build/jacocoHtml/index.html
3. Open index.html in a browser to view test results